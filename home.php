<?php include('header-login.php'); ?>



<style type="text/css">
	#header {
		background: #1491FF;
	}
	.nav-main ul#topMain li a{
		color: #fff;
	}
	.nav-main ul#topMain li a:hover {
		color: #ddd;
	}
	.text-center {
		margin-top: 10%;
	}
	#blog {
		width: 100%;
	}
	section.answer, section.errorAnswer {
		display: none;
	}

	
</style>
<section>
	<div class="container">

			<!--	<ul class="nav nav-tabs nav-justified">
					<li class="active"><a style="color:#fff;border:0px solid transparent;" href="#home" data-toggle="tab">Home</a></li>
					<li><a style="color:#fff;border:0px solid transparent;" href="#profile" data-toggle="tab">Profile</a></li>
				</ul>   -->

				<div class="text-center">
						<h1 class="nomargin">Askit ng</h1>
				</div>

				<div class="col-md-3"></div>
				<div class="col-md-6">
					<form class="nomargin smartForm" action="" method="post">
							<div class="input-group">
								<input type="text" id="search" name="question" class="form-control required" placeholder="Ask anything...">
								
								<span class="input-group-btn">
								<div class="btn-group">
								<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">SHOW ALL</button>
								<ul class="dropdown-menu" role="menu">
								<li><a href="#"> SHOW ALL</a></li>
								<li><a href="#"> BEST</a></li>
								<li><a href="#"> EXPERTS </a></li>
								<!--<li class="divider"></li>
								<li><a href="#"><i class="fa fa-cogs"></i> Separated link</a></li>-->
								</ul>
								</div>
								</span>

								<span class="input-group-btn">
									<button class="btn btn-success" type="submit" id="smartSubmit"><i class="fa fa-search"></i></button>
								</span>
							</div>
						</form>


						<div class="text-center">
							<p><a href="#">Take a tour of Askit in 5 minutes</a></p>
						</div>
				</div>
				<div class="col-md-3"></div>


				


	</div>
</section>


<section class="answer">
	<div class="container">

				<div class="col-md-12">

						<div class="col-md-1"></div>
						<div class="col-md-10">

							

							<div class="col-md-6">
								<div class="shade">
								<p>You melt wax with heat...<br>
								Try dropping it in a hot frying pan</p>

								<ul class="blog-post-info list-inline">
								<li>
									<a href="javascript:;">
										<span class="font-lato">Rate this</span>
										<i class="fa fa-star"></i> 
										
									</a>
								</li>
							</ul>
								</div>
							</div>


							<div class="col-md-6">
							<div class="shade">
								<p>If you are melting wax for remolding; a 
									little heat can do the trick...<br><br>

									If you are melting wax to liquid; trying placing 
									on a metal plate being heated...</p>

								<ul class="blog-post-info list-inline">
								<li>
									<a href="javascript:;">
										<span class="font-lato">Rate this</span>
										<i class="fa fa-star"></i> 
										
									</a>
								</li>
							</ul>
							</div>
							</div>

							<div class="col-md-6">
							<div class="shade">
								<p>Do you see a candle being lit? The wax 
									molded around the thread melts as the 
									frames burn the thread...so I’d say fire is a 
									good and effective way to melt wax.</p>

								<ul class="blog-post-info list-inline">
								<li>
									<a href="javascript:;">
										<span class="font-lato">Rate this</span>
										<i class="fa fa-star"></i> 
										
									</a>
								</li>
							</ul>
							</div>
							</div>

							<div class="col-md-6">
							<div class="shade">
								<p>Do you see a candle being lit? The wax 
									molded around the thread melts as the 
									frames burn the thread...so I’d say fire is a 
									good and effective way to melt wax.</p>

								<ul class="blog-post-info list-inline">
								<li>
									<a href="javascript:;">
										<span class="font-lato">Rate this</span>
										<i class="fa fa-star"></i> 
										
									</a>
								</li>
							</ul>
							</div>
							</div>


						</div>
						<div class="col-md-1"></div>

				</div>

	</div>
</section>



<section class="errorAnswer">
	<div class="container">

				<div class="col-md-12">

						<div class="col-md-1"></div>
						<div class="col-md-10">

							<div class="shade">
								<center><h2>Askit is having issues answering this question!</h2>
								<p>Try asking an Expert</p></center>

								
							</div>


						</div>
						<div class="col-md-1"></div>

				</div>

	</div>
</section>


<script type="text/javascript">
		$(document).ready(function() {

			$('.dropdown-toggle').css("color", "#fff");


			$('.smartForm').submit(function(e) {
					e.preventDefault();
					$('.text-center').fadeOut(300);
					$("#header").css("background", "#fff");
					$(".nav-main ul#topMain li a, .nav-main ul#topMain li a i").css("color", "#444");
					$("section.answer").fadeIn(300);
					$('.dropdown-toggle').css("color", "#444");
					$('.btn-danger').css("color", "#fff");

			});

			$('section.answer').click(function() {
					$("section.errorAnswer").fadeIn(300);
					$("section.answer").fadeOut(300);
			});
		});

</script>



<?php include('footer.php'); ?>






<?php include('footer.php'); ?>