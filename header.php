
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>askit</title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="Author" content="MIshael Kama" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.1.4.min.js"></script>

		<!-- REVOLUTION SLIDER -->
		<link href="assets/plugins/slider.revolution.v5/css/pack.css" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
	</head>



	<body class="smoothscroll enable-animation">

		<!-- wrapper -->
		<div id="wrapper">


		<div id="header" class="header-sm static clearfix noborder">

				<!-- TOP NAV -->
				<header id="topNav">
					<div class="container">

						<!-- Mobile Menu Button -->
						<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
							<i class="fa fa-bars"></i>
						</button>


						<!-- Logo -->
						<a class="logo pull-left" href="/">
							<img src="#" alt="" />
						</a>

						
						<div class="navbar-collapse pull-right nav-main-collapse collapse">
							<nav class="nav-main">

								<ul id="topMain" class="nav nav-pills nav-main">
									<li class="tour">
										<a href="tour">Take a TOUR</a>
									</li>
									<li>
										<a href="login">LOGIN</a>
									</li>

									<li>
										<a href="login">SIGN UP</a>
									</li>
					
									
								</ul>

							</nav>
						</div>

					</div>
				</header>
				<!-- /Top Nav -->

			</div>


<style type="text/css">
	section, body {
		background: #1491FF;
		border: 0px solid transparent;
	}
	
	.text-center .nomargin {
		color: #fff;
		font-size: 60px;
	}
	.copyright ul li a {
		color: #666;
	}
	.wrapper {
		min-height: 100%;
	}
	.footerhead {
		
	}
	.text-center p a{
		color: #fff;
	}
	.shade {
		color: #fff;
		background: #2D8DD6;
		width: 90%;
		padding: 2.5%;
		margin-bottom: 10px;
		padding-bottom: 3px;
		margin-left: 5%;
	}
	.shade ul {
		border-bottom: 0px solid transparent;
		max-height: 10px;
	}
	.shade ul li {
		max-height: 20px;
	}
	.shade ul li a, .shade ul li i {
		color: #fff;
	}
	section.answer, section.errorAnswer {
		padding-top: 0px;
	}
	section.answer .container, section.errorAnswer .container {
		padding-top: 0px;
	}
	section.errorAnswer h2 {
		color: #fff;
	}

</style>