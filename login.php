<?php include('header.php'); ?>


<style type="text/css">
	*::-webkit-input-placeholder { /* WebKit, Blink, Edge */
    color:    #fff;
    opacity: 0.6;
}
*:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
   color:    #fff;
   opacity:  0.5;
}
*::-moz-placeholder { /* Mozilla Firefox 19+ */
   color:    #fff;
   opacity:  0.6;
}
*:-ms-input-placeholder { /* Internet Explorer 10-11 */
   color:    #fff;
   opacity: 0.6;
}

</style>



<section>
	<div class="container">

			<div class="col-md-3"></div>
			<div class="col-md-6">
				


						<div class="box-static box-border-top padding-30 shade" style="border-top:0px solid transparent;margin-top:15%;">
								

								<form class="nomargin" method="post" action="#" autocomplete="off">
									<div class="clearfix">
										
										<!-- Email -->
										<div class="form-group">
											<input  style="font-size:20px;background:#237ECD;color:#fff;border:0px solid transparent;" type="text" name="email" class="form-control" placeholder="Email" required="">
										</div>
										
										<!-- Password -->
										<div class="form-group">
											<input  style="font-size:20px;background:#237ECD;color:#fff;border:0px solid transparent;" type="password" name="password" class="form-control" placeholder="Password" required="">
										</div>
											
									</div>
									
									<div class="row">
										
										<div class="col-md-6 col-sm-6 col-xs-6">

											<button class="btn btn-primary" style="background:transparent; color:#fff;border:0px solid transparent;font-size: 30px;">LOG IN</button>

										</div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-right">
											
											<!-- Inform Tip -->                                        
											<div class="form-tip pt-20">
												<a class="no-text-decoration size-13 margin-top-10 block"   style="color:#fff;" href="#">Forgot Password?</a>
											</div>
											
										</div>
										
										
									</div>
									
								</form>

							</div>


							<div class="text-center">
							<p><a href="#">Don't have an account? Sign up</a></p>
						</div>



			</div>
			<div class="col-md-3"></div>

	</div>
</section>




<?php include('footer.php'); ?>